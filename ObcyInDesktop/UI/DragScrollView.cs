﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace ObcyInDesktop.UI
{
    public class DragScrollView : ScrollViewer
    {
        private class Vector
        {
            public Vector(double x, double y)
            {
                X = x;
                Y = y;
            }

            public double X { get; private set; }
            public double Y { get; private set; }

            public double Length
            {
                get { return Math.Sqrt(X * X + Y * Y); }
            }

            public static Vector operator *(Vector vector, double scalar)
            {
                return new Vector(vector.X * scalar, vector.Y * scalar);
            }
        }

        private const double DragPollingInterval = 10;
        private const double DefaultFriction = 0.2;
        private const double MinimumFriction = 0.0;
        private const double MaximumFriction = 1.0;

        private double friction = DefaultFriction;

        private Point previousPreviousPoint;
        private Point previousPoint;
        private Point currentPoint;

        private DispatcherTimer dragScrollTimer;

        private bool mouseDown;
        private bool isDragging;

        public double Friction
        {
            get
            {
                return friction;
            }
            set
            {
                friction = Math.Min(
                    Math.Max(value, MinimumFriction), MaximumFriction
                );
            }
        }

        private Vector Velocity
        {
            get
            {
                return new Vector(currentPoint.X - previousPoint.X, currentPoint.Y - previousPoint.Y);
            }
        }

        private Vector PreviousVelocity
        {
            get
            {
                return new Vector(previousPoint.X - previousPreviousPoint.X, previousPoint.Y - previousPreviousPoint.Y);
            }
        }

        private Vector Momentum { get; set; }

        private void BeginDrag()
        {
            mouseDown = true;
        }

        private void CancelDrag(Vector velocityToUse)
        {
            if (isDragging)
                Momentum = velocityToUse;

            isDragging = false;
            mouseDown = false;
        }

        private void TickDragScroll(object sender, EventArgs e)
        {
            if (isDragging)
            {

                GeneralTransform generalTransform = TransformToVisual(this);
                Point childToParentCoordinates = generalTransform.Transform(new Point(0, 0));
                Rect bounds = new Rect(childToParentCoordinates, RenderSize);

                if (bounds.Contains(currentPoint))
                {
                    PerformScroll(PreviousVelocity);
                }

                if (!mouseDown)
                {
                    CancelDrag(Velocity);
                }
                previousPreviousPoint = previousPoint;
                previousPoint = currentPoint;
            }
            else if (Momentum.Length > 0)
            {
                Momentum *= (1.0 - friction/4.0);
                PerformScroll(Momentum);
            }
            else
            {
                if (dragScrollTimer != null)
                {
                    dragScrollTimer.Tick -= TickDragScroll;
                    dragScrollTimer.Stop();
                    dragScrollTimer = null;
                }
            }
        }

        private void PerformScroll(Vector displacement)
        {
            var verticalOffset = Math.Max(0.0, VerticalOffset - displacement.Y);
            ScrollToVerticalOffset(verticalOffset);

            var horizontalOffset = Math.Max(0.0, HorizontalOffset - displacement.X);
            ScrollToHorizontalOffset(horizontalOffset);
        }

        protected void DragScroll()
        {
            if (dragScrollTimer == null)
            {
                dragScrollTimer = new DispatcherTimer();
                dragScrollTimer.Tick += TickDragScroll;
                dragScrollTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)DragPollingInterval);
                dragScrollTimer.Start();
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            CancelDrag(PreviousVelocity);
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            currentPoint = previousPoint = previousPreviousPoint = e.GetPosition(this);
            Momentum = new Vector(0, 0);
            BeginDrag();
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            currentPoint = e.GetPosition(this);
            if (mouseDown && !isDragging)
            {
                isDragging = true;
                DragScroll();
            }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            CancelDrag(PreviousVelocity);
        }
    }
}
