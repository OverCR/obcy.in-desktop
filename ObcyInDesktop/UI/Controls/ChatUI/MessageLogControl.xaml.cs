﻿using System;
using System.Windows;
using ObcyProtoRev.Protocol.Client;

namespace ObcyInDesktop.UI.Controls.ChatUI
{
    public partial class MessageLogControl
    {
        public bool ScrollOnMessage { get; set; }

        public MessageLogControl()
        {
            InitializeComponent();
            ScrollOnMessage = true;
        }

        public void AddMessage(Message message, bool incoming)
        {
            MessageControl mc = new MessageControl(incoming, message.Body);
            MessagePanel.Children.Add(mc);

            if (ScrollOnMessage)
            {
                ScrollView.ScrollToEnd();
            }
        }

        public void AddMessage(Message message, bool incoming, DateTime creationTime)
        {
            MessageControl mc = new MessageControl(incoming, message.Body, creationTime);
            MessagePanel.Children.Add(mc);

            if (ScrollOnMessage)
            {
                ScrollView.ScrollToEnd();
            }
        }

        public void AddTopic(Message message)
        {
            TopicControl tc = new TopicControl(message.Body);
            MessagePanel.Children.Add(tc);

            if (ScrollOnMessage)
            {
                ScrollView.ScrollToEnd();
            }
        }

        public void AddPresence(bool started)
        {
            PresenceControl pc = new PresenceControl(started);
            MessagePanel.Children.Add(pc);

            if (ScrollOnMessage)
            {
                ScrollView.ScrollToEnd();
            }
        }

        public void ToggleCopyView()
        {
            switch (CopyView.Visibility)
            {
                case Visibility.Visible:
                    CopyView.Visibility = Visibility.Collapsed;
                    CopyViewScrollViewer.Visibility = Visibility.Collapsed;
                    break;
                default:
                    CopyView.ScrollToEnd();
                    CopyView.Visibility = Visibility.Visible;
                    CopyViewScrollViewer.Visibility = Visibility.Visible;
                    break;
            }
            FillCopyView();
        }

        private void FillCopyView()
        {
            CopyView.Clear();

            foreach (UIElement e in MessagePanel.Children)
            {
                if (e is TopicControl)
                {
                    var tc = e as TopicControl;
                    CopyView.AppendText(string.Format("{0}: {1}\n", tc.TopicTitleTextBlock.Text, tc.Topic));
                }

                if (e is PresenceControl)
                {
                    var pc = e as PresenceControl;
                    CopyView.AppendText(string.Format("[{0}] {1}\n", pc.CreationTime, pc.PresenceTextBlock.Text));
                }

                if (e is MessageControl)
                {
                    var mc = e as MessageControl;
                    CopyView.AppendText(
                        string.Format("[{0}] {1}: {2}\n", mc.TimeTextBlock.Text, mc.SenderTextBlock.Text, mc.MessageTextBlock.Text)
                    );
                }
            }
        }
    }
}
