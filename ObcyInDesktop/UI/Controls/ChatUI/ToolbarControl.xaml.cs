﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ObcyInDesktop.UI.Controls.ChatUI
{
    public partial class ToolbarControl
    {
        private readonly ImageSource chatLogImage;
        private readonly ImageSource scrollStopImage;
        private readonly ImageSource eraseLogImage;
        private readonly ImageSource flagStrangerImage;
        private readonly ImageSource randomTopicImage;
        private readonly ImageSource disconnectImage;
        private readonly ImageSource searchImage;

        public ToolbarControl()
        {
            InitializeComponent();

            if (App.ColorSchemeLoader.CurrentSchemeUsesDarkIcons)
            {
                chatLogImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/ChatLog.png")
                );

                scrollStopImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/ScrollStop.png")
                );

                eraseLogImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/EraseLog.png")
                );

                flagStrangerImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/FlagStranger.png")
                );

                randomTopicImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/RandomTopic.png")
                );

                disconnectImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/Disconnect.png")
                );

                searchImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Dark/Search.png")
                );
            }

            if (App.ColorSchemeLoader.CurrentSchemeUsesLightIcons)
            {
                chatLogImage =
                    new BitmapImage(
                        new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/ChatLog.png")
                    );

                scrollStopImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/ScrollStop.png")
                );

                eraseLogImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/EraseLog.png")
                );

                flagStrangerImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/FlagStranger.png")
                );

                randomTopicImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/RandomTopic.png")
                );

                disconnectImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/Disconnect.png")
                );

                searchImage =
                    new BitmapImage(new Uri("pack://application:,,,/Resources/Graphics/Toolbar/Light/Search.png")
                );
            }

            CopyViewToggleButton.Content = new Image
            {
                Source = chatLogImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            LogScrollingToggleButton.Content = new Image
            {
                Source = scrollStopImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            ClearLogButton.Content = new Image
            {
                Source = eraseLogImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            FlagStrangerButton.Content = new Image
            {
                Source = flagStrangerImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            RequestRandomTopicButton.Content = new Image
            {
                Source = randomTopicImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            DisconnectFromStrangerButton.Content = new Image
            {
                Source = disconnectImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };

            SearchForStrangerButton.Content = new Image
            {
                Source = searchImage,
                Height = 16,
                Width = 16,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                SnapsToDevicePixels = true
            };
        }
    }
}
