﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using ObcyInDesktop.Localization;
using ObcyInDesktop.Settings;
using ObcyProtoRev.Protocol;
using ObcyProtoRev.Protocol.Client;
using ObcyProtoRev.Protocol.Client.Identity;

namespace ObcyInDesktop.UI.Controls.ChatUI
{
    public partial class ChatView
    {
        private readonly Connection connection;

        private DoubleAnimation gridFadeOutAnimation;
        private DoubleAnimation gridFadeInAnimation;
        private DoubleAnimation chatBoxHeightUpAnimation;
        private DoubleAnimation chatBoxHeightDownAnimation;
        
        private bool typing;

        public ChatView()
        {
            InitializeComponent();
        }

        public ChatView(Connection connection)
            : this()
        {
            this.connection = connection;

            InitializeAnimations();
            InitializeConnection();

            Connect();
        }

        public event EventHandler MessageSent;

        private void InitializeAnimations()
        {
            gridFadeOutAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
            gridFadeInAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
            chatBoxHeightUpAnimation = new DoubleAnimation(ChatBox.Height, ChatBox.Height + 25, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
            chatBoxHeightDownAnimation = new DoubleAnimation(ChatBox.Height + 25, ChatBox.Height, new Duration(new TimeSpan(0, 0, 0, 0, 200)));

            gridFadeOutAnimation.Completed += GridFadeOutAnimation_Completed;
            chatBoxHeightDownAnimation.Completed += ChatBoxHeightDownAnimation_Completed;
        }

        private void InitializeConnection()
        {
            connection.StrangerFound += Connection_StrangerFound;
            connection.ConversationEnded += Connection_ConversationEnded;
            connection.MessageReceived += Connection_MessageReceived;
            connection.StrangerChatstateChanged += Connection_StrangerChatstateChanged;
            connection.ConnectionAccepted += Connection_ConnectionAccepted;
            connection.ServerClosedConnection += connection_ServerClosedConnection;
            connection.SocketClosed += connection_SocketClosed;
            connection.SocketError += Connection_SocketError;
        }

        private void connection_SocketClosed(object sender, string value)
        {
            ConnectionFailureCleanup();
        }

        private void connection_ServerClosedConnection(object sender, EventArgs e)
        {
            ServerDisconnectionCleanup();
        }

        public void ClearLog()
        {
            MessageLog.MessagePanel.Children.Clear();
            MessageLog.CopyView.Clear();
        }

        public void Connect()
        {
            connection.Open();
        }

        public void DisconnectFromStranger()
        {
            if (connection.IsStrangerConnected)
            {
                connection.DisconnectStranger();
            }
            Application.Current.Dispatcher.Invoke(() => StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_ConnectedToServer"));
        }

        public void FlagStranger()
        {
            if (connection.IsStrangerConnected)
            {
                connection.FlagStranger();
            }
        }

        public void RequestRandomTopic()
        {
            if (connection.IsStrangerConnected)
            {
                connection.RequestRandomTopic();
            }
        }

        public void SearchForStranger()
        {
            if (!connection.IsStrangerConnected)
            {
                connection.SearchForStranger((Location)SettingsSelector.GetConfigurationValue<int>("Voivodeship"));
            }
        }

        public void ToggleCopyView()
        {
            MessageLog.ToggleCopyView();
        }

        public void ToggleLogScrolling()
        {
            MessageLog.ScrollOnMessage = !MessageLog.ScrollOnMessage;
        }

        public void ToggleToolbar()
        {
            switch (ToolbarGrid.Visibility)
            {
                case Visibility.Visible:
                    ToolbarGrid.BeginAnimation(OpacityProperty, gridFadeOutAnimation);
                    break;
                default:
                    ToolbarGrid.Visibility = Visibility.Visible;
                    ChatBox.BeginAnimation(HeightProperty, chatBoxHeightDownAnimation);
                    break;
            }
        }

        public void SendMessage(string message)
        {
            connection.SendMessage(message);
        }

        private void ConnectionFailureCleanup()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                DisableChatRelatedControls();
                StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_ConnectionFailure");
            });
        }

        private void DisableChatRelatedControls()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                ChatBox.IsEnabled = false;

                Toolbar.SearchForStrangerButton.IsEnabled = false;
                Toolbar.DisconnectFromStrangerButton.IsEnabled = false;
                Toolbar.FlagStrangerButton.IsEnabled = false;
                Toolbar.RequestRandomTopicButton.IsEnabled = false;
            });
        }

        private void ServerDisconnectionCleanup()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                DisableChatRelatedControls();
                StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_ServerClosedConnection");
            });
        }

        private void ChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrWhiteSpace(ChatBox.Text) && connection.IsStrangerConnected)
            {
                connection.SendMessage(ChatBox.Text);

                if (SettingsSelector.GetConfigurationValue<bool>("Behavior_SendChatstate"))
                {
                    connection.ReportChatstate(false);
                    typing = false;
                }

                MessageLog.AddMessage(new Message(MessageType.Chat, ChatBox.Text, null, null), false);
                ChatBox.Clear();

                OnMessageSent();
            }
        }

        private void ChatBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if(SettingsSelector.GetConfigurationValue<bool>("Behavior_SendChatstate"))
            {
                if (e.Key != Key.Enter && connection.IsStrangerConnected)
                {
                    if (e.Key == Key.Back && typing)
                    {
                        connection.ReportChatstate(false);
                        typing = false;
                    }

                    if ((char.IsLetterOrDigit((char) e.Key) || char.IsPunctuation((char) e.Key) ||
                         char.IsSeparator((char) e.Key)) && !typing)
                    {
                        connection.ReportChatstate(true);
                        typing = true;
                    }
                }
            }
        }

        private void Connection_ConnectionAccepted(object sender, string value)
        {
            Application.Current.Dispatcher.Invoke(() => StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_ConnectedToServer"));
        }

        private void Connection_ConversationEnded(object sender, DisconnectInfo disconnectInfo)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (!disconnectInfo.IsReminder)
                {
                    MessageLog.AddPresence(false);
                }

                ChatBox.Clear();
                ChatBox.IsEnabled = false;

                StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_ConnectedToServer");

                Toolbar.DisconnectFromStrangerButton.IsEnabled = false;
                Toolbar.SearchForStrangerButton.IsEnabled = true;
                Toolbar.FlagStrangerButton.IsEnabled = false;
                Toolbar.RequestRandomTopicButton.IsEnabled = false;
            });
        }

        private void Connection_MessageReceived(object sender, Message message)
        {
            if (message.Type == MessageType.Chat)
            {
                Application.Current.Dispatcher.Invoke(() => MessageLog.AddMessage(message, true));
            }
            else if (message.Type == MessageType.Topic)
            {
                Application.Current.Dispatcher.Invoke(() => MessageLog.AddTopic(message));
            }
        }

        private void Connection_SocketError(object sender, Exception e)
        {
            connection.Close();
        }

        private void Connection_StrangerChatstateChanged(object sender, bool value)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                StatusTextBlock.Text = value
                    ? LocaleSelector.GetLocaleString("StatusIndicator_StrangerTyping")
                    : LocaleSelector.GetLocaleString("StatusIndicator_StrangerIdle");
            });
        }

        private void Connection_StrangerFound(object sender, ContactInfo contactInfo)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                ChatBox.IsEnabled = true;
                ChatBox.Focus();

                MessageLog.AddPresence(true);
                StatusTextBlock.Text = LocaleSelector.GetLocaleString("StatusIndicator_StrangerIdle");

                Toolbar.FlagStrangerButton.IsEnabled = true;
                Toolbar.DisconnectFromStrangerButton.IsEnabled = true;
                Toolbar.RequestRandomTopicButton.IsEnabled = true;
            });
        }

        private void GridFadeOutAnimation_Completed(object sender, EventArgs e)
        {
            ToolbarGrid.Visibility = Visibility.Hidden;
            ChatBox.BeginAnimation(HeightProperty, chatBoxHeightUpAnimation);
        }

        private void ChatBoxHeightDownAnimation_Completed(object sender, EventArgs e)
        {
            ToolbarGrid.BeginAnimation(OpacityProperty, gridFadeInAnimation);
        }

        protected virtual void OnMessageSent()
        {
            var handler = MessageSent;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private void MessageLog_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left &&
                SettingsSelector.GetConfigurationValue<bool>("Behavior_CopyViewOnLogDoubleClick"))
            {
                MessageLog.ToggleCopyView();
                Toolbar.CopyViewToggleButton.IsChecked = !Toolbar.CopyViewToggleButton.IsChecked;
            }
        }
    }
}
