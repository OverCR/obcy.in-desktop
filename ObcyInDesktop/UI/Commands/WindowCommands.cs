﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ObcyInDesktop.UI.Commands
{
    public class WindowCommands
    {
        private static double prevX;
        private static double prevY;
        private static double prevWidth;
        private static double prevHeight;

        private static Window parent;
        private static Button button;
        private static Border captionBar;
        private static Border menuContainer;
        private static Grid leftPanelArea;
        private static Border rightPanelArea;

        private static bool isMaximized = false;

        private static ThicknessAnimation marginCollapseAnimation = new ThicknessAnimation(
            new Thickness(180, 6, 6, 0),
            new Thickness(6, 6, 6, 0),
            new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static ThicknessAnimation marginIncreaseAnimation = new ThicknessAnimation(
            new Thickness(6, 6, 6, 0),
            new Thickness(180, 6, 6, 0),
            new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static ThicknessAnimation rightPanelMarginCollapseAnimation = new ThicknessAnimation(
            new Thickness(174, 25, 0, 0),
            new Thickness(0, 25, 0, 0),
            new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static ThicknessAnimation rightPanelMarginIncreaseAnimation = new ThicknessAnimation(
            new Thickness(0, 25, 0, 0),
            new Thickness(174, 25, 0, 0),
            new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static DoubleAnimation leftPanelFadeOutAnimation = new DoubleAnimation(
            1, 0, new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static DoubleAnimation leftPanelFadeInAnimation = new DoubleAnimation(
            0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static DoubleAnimation menuContainerFadeOutAnimation = new DoubleAnimation(
            1, 0, new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        private static DoubleAnimation menuContainerFadeInAnimation = new DoubleAnimation(
            0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 200))
        );

        public static readonly ICommand CloseCommand = new RelayCommand(o =>
        {
            var parent = o as Window;
            Debug.Assert(parent != null, "parent != null");

            parent.Close();
        });

        public static readonly ICommand MaximizeCommand = new RelayCommand(o =>
        {
            var parent = o as Window;
            Debug.Assert(parent != null, "parent != null");

            switch (isMaximized)
            {
                case true:
                    parent.Left = prevX;
                    parent.Top = prevY;
                    prevX = 0;
                    prevY = 0;

                    parent.Width = prevWidth;
                    parent.Height = prevHeight;

                    isMaximized = false;
                    break;
                default:
                    prevX = parent.Left;
                    prevY = parent.Top;
                    prevWidth = parent.Width;
                    prevHeight = parent.Height;

                    parent.Left = 0;
                    parent.Top = 0;

                    parent.Width = SystemParameters.WorkArea.Width;
                    parent.Height = SystemParameters.WorkArea.Height;

                    isMaximized = true;
                    break;
            }
        });

        public static readonly ICommand MinimizeCommand = new RelayCommand(o =>
        {
            var parent = o as Window;
            Debug.Assert(parent != null, "parent != null");

            parent.WindowState = WindowState.Minimized;
        });

        public static readonly ICommand DragWindow = new RelayCommand(o =>
        {
            var parent = o as Window;
            Debug.Assert(parent != null, "parent != null");

            parent.DragMove();
        });

        public static readonly ICommand TogglePanel = new RelayCommand(o =>
        {
            parent = o as Window;
            Debug.Assert(parent != null, "parentWindow != null");

            leftPanelArea = parent.FindName("LeftPanelArea") as Grid;
            Debug.Assert(leftPanelArea != null, "leftPanelArea != null");

            rightPanelArea = parent.FindName("RightPanelArea") as Border;
            Debug.Assert(rightPanelArea != null, "rightPanelArea != null");

            menuContainer = parent.FindName("MenuContainer") as Border;
            Debug.Assert(menuContainer != null, "menuContainer != null");

            var captionBarDependencyObject = parent.Template.FindName("CaptionBar", parent) as DependencyObject;
            Debug.Assert(captionBarDependencyObject != null, "captionBarDependencyObject != null");

            var captionBarParentGrid = VisualTreeHelper.GetChild(captionBarDependencyObject, 0) as Grid;
            Debug.Assert(captionBarParentGrid != null, "captionBarParentGrid != null");

            captionBar = (captionBarParentGrid).FindName("CaptionBar") as Border;
            Debug.Assert(captionBar != null, "captionBar != null");

            button = captionBarParentGrid.FindName("HidePanelButton") as Button;
            Debug.Assert(button != null, "button != null");

            if (captionBar.Margin == new Thickness(6, 6, 6, 0))
            {
                marginIncreaseAnimation.Completed += marginIncreaseAnimation_Completed;
                rightPanelMarginIncreaseAnimation.Completed += rightPanelMarginIncreaseAnimation_Completed;

                captionBar.BeginAnimation(FrameworkElement.MarginProperty, marginIncreaseAnimation);
                rightPanelArea.BeginAnimation(FrameworkElement.MarginProperty, rightPanelMarginIncreaseAnimation);
            }
            else
            {
                leftPanelFadeOutAnimation.Completed += leftPanelFadeOutAnimation_Completed;
                menuContainerFadeOutAnimation.Completed += menuContainerFadeOutAnimation_Completed;

                leftPanelArea.BeginAnimation(UIElement.OpacityProperty, leftPanelFadeOutAnimation);
                menuContainer.BeginAnimation(UIElement.OpacityProperty, menuContainerFadeOutAnimation);
            }
        });

        static void rightPanelMarginIncreaseAnimation_Completed(object sender, EventArgs e)
        {
            rightPanelMarginIncreaseAnimation.Completed -= rightPanelMarginIncreaseAnimation_Completed;
        }

        static void menuContainerFadeOutAnimation_Completed(object sender, EventArgs e)
        {
            menuContainer.Visibility = Visibility.Collapsed;
            menuContainerFadeOutAnimation.Completed -= menuContainerFadeOutAnimation_Completed;
        }

        static void leftPanelFadeOutAnimation_Completed(object sender, EventArgs e)
        {
            marginCollapseAnimation.Completed += marginCollapseAnimation_Completed;
            rightPanelMarginCollapseAnimation.Completed += rightPanelMarginCollapseAnimation_Completed;

            captionBar.BeginAnimation(FrameworkElement.MarginProperty, marginCollapseAnimation);
            rightPanelArea.BeginAnimation(FrameworkElement.MarginProperty, rightPanelMarginCollapseAnimation);

            leftPanelFadeOutAnimation.Completed -= leftPanelFadeOutAnimation_Completed;
        }

        static void rightPanelMarginCollapseAnimation_Completed(object sender, EventArgs e)
        {
            rightPanelMarginCollapseAnimation.Completed -= rightPanelMarginCollapseAnimation_Completed;
        }

        private static void marginIncreaseAnimation_Completed(object sender, EventArgs e)
        {
            button.LayoutTransform = new RotateTransform(0);

            leftPanelArea.Width = 174;
            leftPanelArea.Visibility = Visibility.Visible;

            menuContainerFadeInAnimation.Completed += menuContainerFadeInAnimation_Completed;
            leftPanelFadeInAnimation.Completed += leftPanelFadeInAnimation_Completed;

            menuContainer.Visibility = Visibility.Visible;

            menuContainer.BeginAnimation(UIElement.OpacityProperty, menuContainerFadeInAnimation);
            leftPanelArea.BeginAnimation(UIElement.OpacityProperty, leftPanelFadeInAnimation);

            marginIncreaseAnimation.Completed -= marginIncreaseAnimation_Completed;
        }

        static void leftPanelFadeInAnimation_Completed(object sender, EventArgs e)
        {
            leftPanelFadeInAnimation.Completed -= leftPanelFadeInAnimation_Completed;
        }

        static void menuContainerFadeInAnimation_Completed(object sender, EventArgs e)
        {
            menuContainerFadeInAnimation.Completed -= menuContainerFadeInAnimation_Completed;
        }

        private static void marginCollapseAnimation_Completed(object sender, EventArgs eventArgs)
        {
            button.LayoutTransform = new RotateTransform(180);

            leftPanelArea.Width = 0;
            leftPanelArea.Visibility = Visibility.Collapsed;

            marginCollapseAnimation.Completed -= marginCollapseAnimation_Completed;
        }
    }
}
