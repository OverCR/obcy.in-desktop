﻿using System.Windows;
using ObcyProtoRev.Protocol.Client;

namespace ObcyInDesktop.Archive
{
    class ConversationEventListener
    {
        private ArchiveManager ArchiveManager { get; set; }

        public ConversationEventListener(ArchiveManager archiveManager)
        {
            ArchiveManager = archiveManager;

            App.Connection.StrangerFound += Connection_StrangerFound;
            App.Connection.ConversationEnded += Connection_ConversationEnded;
            App.Connection.MessageReceived += Connection_MessageReceived;
            App.Connection.MessageSent += Connection_MessageSent;

            Application.Current.Exit += Current_Exit;
        }

        private void Connection_StrangerFound(object sender, ContactInfo contactInfo)
        {
            ArchiveManager.ConversationStart();
        }

        private void Connection_ConversationEnded(object sender, DisconnectInfo disconnectInfo)
        {
            if (!disconnectInfo.IsReminder)
            {
                ArchiveManager.ConversationEndCleanup();
            }
        }

        private void Connection_MessageReceived(object sender, Message message)
        {
            switch (message.Type)
            {
                case MessageType.Service:
                    return;
                case MessageType.Chat:
                    ArchiveManager.AddMessage(true, message.Body);
                    break;
                case MessageType.Topic:
                    ArchiveManager.AddTopic(message.Body);
                    break;
            }
        }

        private void Connection_MessageSent(object sender, Message message)
        {
            ArchiveManager.AddMessage(false, message.Body);
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            ArchiveManager.ConversationEndCleanup();
        }
    }
}
