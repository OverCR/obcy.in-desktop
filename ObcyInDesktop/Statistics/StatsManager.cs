﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Timers;
using ObcyInDesktop.Filesystem;
using ObcyProtoRev.Protocol;
using ObcyProtoRev.Protocol.Client;

namespace ObcyInDesktop.Statistics
{
    public class StatsManager
    {
        private readonly Connection connection;
        private Timer conversationTimer;
        private bool kilometerAddedInCurrentConversation;
        private bool recordingStats;

        public StatsManager(Connection connection)
        {
            this.connection = connection;
            Statistics = new Stats();

            CreateConversationTimer();
            InitializeConnectionEvents();

            recordingStats = true;
        }

        public event EventHandler BestConversationTimeChanged;
        public event EventHandler ConversationCountChanged;
        public event EventHandler CurrentConversationTimeChanged;
        public event EventHandler KilometersCountChanged;
        public event EventHandler ReceivedMessagesCountChanged;
        public event EventHandler SentMessagesCountChanged;

        public Stats Statistics { get; private set; }
        public TimeSpan CurrentConversationTime { get; private set; }

        public void AddSentMessage()
        {
            Statistics.MessagesSent += 1;
            OnSentMessagesCountChanged();
        }

        public void LoadStats(string filePath)
        {
            using (var deflateStream = new DeflateStream(File.OpenRead(filePath), CompressionMode.Decompress))
            {
                var binaryFormatter = new BinaryFormatter();
                var stats = binaryFormatter.Deserialize(deflateStream) as Stats;

                if (stats != null)
                {
                    Statistics = stats;

                    OnBestConversationTimeChanged();
                    OnConversationCountChanged();
                    OnKilometersCountChanged();
                    OnReceivedMessagesCountChanged();
                    OnSentMessagesCountChanged();
                }
            }
        }

        public void SaveStats(string filePath)
        {
            using (var deflateStream = new DeflateStream(File.OpenWrite(filePath), CompressionMode.Compress))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(deflateStream, Statistics);
            }
        }

        public void ClearStats()
        {
            Statistics.BestConversationTime = TimeSpan.Zero;
            Statistics.ConversationCount = 0;
            Statistics.KilometersCount = 0;
            Statistics.MessagesReceived = 0;
            Statistics.MessagesSent = 0;

            SaveStats(
                Path.Combine(DirectoryGuard.DataDirectory, "statistics.dat")
            );

            OnBestConversationTimeChanged();
            OnConversationCountChanged();
            OnKilometersCountChanged();
            OnReceivedMessagesCountChanged();
            OnSentMessagesCountChanged();
        }

        public void StopRecording()
        {
            conversationTimer.Stop();
            recordingStats = false;
        }

        private void CreateConversationTimer()
        {
            conversationTimer = new Timer(1000);
            conversationTimer.Elapsed += conversationTimer_Elapsed;
        }

        private void InitializeConnectionEvents()
        {
            connection.StrangerFound += connection_StrangerFound;
            connection.MessageReceived += connection_MessageReceived;
            connection.ConversationEnded += connection_ConversationEnded;
        }

        private void connection_ConversationEnded(object sender, DisconnectInfo disconnectInfo)
        {
            if (!recordingStats)
                return;

            if (!disconnectInfo.IsReminder)
            {
                conversationTimer.Stop();

                CurrentConversationTime = TimeSpan.Zero;
                OnCurrentConversationTimeChanged();
            }
        }

        private void connection_MessageReceived(object sender, Message message)
        {
            if (!recordingStats)
                return;

            if (message.Type == MessageType.Chat)
            {
                if (Regex.IsMatch(message.Body, @"^*[Kk]\/?[Mm]\b"))
                {
                    if (!kilometerAddedInCurrentConversation)
                    {
                        Statistics.KilometersCount += 1;
                        OnKilometersCountChanged();

                        kilometerAddedInCurrentConversation = true;
                    }
                }
                Statistics.MessagesReceived += 1;
                OnReceivedMessagesCountChanged();
            }
        }

        private void connection_StrangerFound(object sender, ContactInfo contactInfo)
        {
            if (!recordingStats)
                return;

            kilometerAddedInCurrentConversation = false;

            Statistics.ConversationCount += 1;
            OnConversationCountChanged();

            conversationTimer.Start();
        }

        private void conversationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!recordingStats)
                return;

            CurrentConversationTime = CurrentConversationTime.Add(new TimeSpan(0, 0, 0, 1));
            OnCurrentConversationTimeChanged();

            if (CurrentConversationTime > Statistics.BestConversationTime)
            {
                Statistics.BestConversationTime = CurrentConversationTime;
                OnBestConversationTimeChanged();
            }
        }

        protected virtual void OnBestConversationTimeChanged()
        {
            var handler = BestConversationTimeChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnConversationCountChanged()
        {
            var handler = ConversationCountChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnCurrentConversationTimeChanged()
        {
            var handler = CurrentConversationTimeChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnKilometersCountChanged()
        {
            var handler = KilometersCountChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnReceivedMessagesCountChanged()
        {
            var handler = ReceivedMessagesCountChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnSentMessagesCountChanged()
        {
            var handler = SentMessagesCountChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}