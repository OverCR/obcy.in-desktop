﻿using System;
using System.IO;
using ObcyInDesktop.Filesystem;

namespace ObcyInDesktop.Settings
{
    static class SettingsSelector
    {
        public static event EventHandler SettingsChanged;
        private static readonly SettingsManager SettingsManager;

        static SettingsSelector()
        {
            SettingsManager = new SettingsManager();

            var settingsFileParser = new SettingsFileParser(
                Path.Combine(DirectoryGuard.DataDirectory, DirectoryGuard.SettingsFileName),
                SettingsManager
            );

            settingsFileParser.LoadSettings();
            OnSettingsChanged();
        }

        public static void SetConfigurationValue(string key, string value)
        {
            SettingsManager[key] = value;

            SettingsManager.Save(
                Path.Combine(DirectoryGuard.DataDirectory, DirectoryGuard.SettingsFileName)
            );
            OnSettingsChanged();
        }

        public static T GetConfigurationValue<T>(string key)
        {
            return SettingsManager.GetConfigurationValue<T>(key);
        }

        private static void OnSettingsChanged()
        {
            var handler = SettingsChanged;
            if (handler != null) handler(null, EventArgs.Empty);
        }
    }
}
