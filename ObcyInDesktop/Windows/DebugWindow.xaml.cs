﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ObcyInDesktop.Localization;
using ObcyInDesktop.UI;
using ObcyProtoRev.Protocol;

namespace ObcyInDesktop.Windows
{
    public partial class DebugWindow
    {
        private readonly Connection connection;

        private readonly ColorAnimation glowFadeOutAnimation;
        private readonly ColorAnimation glowFadeInAnimation;

        private readonly Storyboard borderActivateStoryboard;
        private readonly Storyboard borderDeactivateStoryboard;

        public DebugWindow()
        {
            InitializeComponent();

            glowFadeOutAnimation = new ColorAnimation(Colors.WindowActiveGlow, Colors.WindowInactiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            glowFadeInAnimation = new ColorAnimation(Colors.WindowInactiveGlow, Colors.WindowActiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            var borderActivateAnimation = new ColorAnimation(Colors.WindowInactiveBorder, Colors.WindowActiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            var borderDeactivateAnimation = new ColorAnimation(Colors.WindowActiveBorder, Colors.WindowInactiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            borderActivateStoryboard = new Storyboard();
            borderDeactivateStoryboard = new Storyboard();

            Storyboard.SetTarget(borderActivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderActivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderActivateStoryboard.Children.Add(borderActivateAnimation);

            Storyboard.SetTarget(borderDeactivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderDeactivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderDeactivateStoryboard.Children.Add(borderDeactivateAnimation);

            Title = LocaleSelector.GetLocaleString("DebugWindow_Title");
        }

        public DebugWindow(Connection connection) : this()
        {
            this.connection = connection;
            connection.JsonRead += connection_JsonRead;
            connection.JsonWritten += connection_JsonWritten;

            var updateTimer = new Timer(200);
            updateTimer.Elapsed += UpdateTimer_OnElapsed;
            updateTimer.Start();
        }

        private void DebugWindow_OnActivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

            borderActivateStoryboard.Begin();
        }

        private void DebugWindow_OnDeactivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

            borderDeactivateStoryboard.Begin();
        }

        void connection_JsonWritten(object sender, string value)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                JsonConsoleTextBox.AppendText(string.Format("OUT\n{0}\n\n", value));
                JsonConsoleTextBox.ScrollToEnd();
            });
        }

        void connection_JsonRead(object sender, string value)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                JsonConsoleTextBox.AppendText(string.Format("IN\n{0}\n\n", value));
                JsonConsoleTextBox.ScrollToEnd();
            });
        }

        private void UpdateTimer_OnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                IsOpenTextBlock.Text = string.Format("IsOpen: {0}", connection.IsOpen);
                IsReadyTextBlock.Text = string.Format("IsReady: {0}", connection.IsReady);
                IsStrangerConnectedTextBlock.Text = string.Format("IsStrangerConnected: {0}", connection.IsStrangerConnected);
                IsSearchingForStrangerTextBlock.Text = string.Format("IsSearchingForStranger: {0}", connection.IsSearchingForStranger);
                StrangerIdTextBlock.Text = string.Format("Stranger ID: {0}", connection.CurrentContactUID);
            });
        }

        private void JsonManualInputTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && JsonManualInputTextBox.Text.Length > 0)
            {
                connection.SendJson(JsonManualInputTextBox.Text);
                JsonManualInputTextBox.Clear();
            }
        }

        private void InsertTemplateHyperlink_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                JsonManualInputTextBox.Text = "[\"{\\\"ev_name\\\":\\\"EVENT_NAME\\\",\\\"ev_data\\\":{EVENT_DATA}}\"]";
            });
        }
    }
}
