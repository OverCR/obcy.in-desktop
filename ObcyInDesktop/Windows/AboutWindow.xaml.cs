﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ObcyInDesktop.Localization;
using ObcyInDesktop.UI;

namespace ObcyInDesktop.Windows
{
    public partial class AboutWindow
    {
        private readonly ColorAnimation glowFadeInAnimation;
        private readonly ColorAnimation glowFadeOutAnimation;

        private readonly Storyboard borderActivateStoryboard;
        private readonly Storyboard borderDeactivateStoryboard;

        private readonly DoubleAnimation fadeOutAnimation;
        private readonly DoubleAnimation fadeInAnimation;

        private readonly DoubleAnimation heightUpAnimation;

        public AboutWindow()
        {
            InitializeComponent();

            Title = LocaleSelector.GetLocaleString("AboutWindow_Title");

            var assemblyName = Assembly.GetExecutingAssembly().GetName();
            
            VersionNumberTextBlock.Text = string.Format(
                VersionNumberTextBlock.Text,
                assemblyName.Version.Major,
                assemblyName.Version.Minor,
                assemblyName.Version.Build,
                assemblyName.Version.Revision
            );
            
            fadeOutAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            fadeInAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            glowFadeOutAnimation = new ColorAnimation(Colors.WindowActiveGlow, Colors.WindowInactiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            glowFadeInAnimation = new ColorAnimation(Colors.WindowInactiveGlow, Colors.WindowActiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            var borderActivateAnimation = new ColorAnimation(Colors.WindowInactiveBorder, Colors.WindowActiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            var borderDeactivateAnimation = new ColorAnimation(Colors.WindowActiveBorder, Colors.WindowInactiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            borderActivateStoryboard = new Storyboard();
            borderDeactivateStoryboard = new Storyboard();

            Storyboard.SetTarget(borderActivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderActivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderActivateStoryboard.Children.Add(borderActivateAnimation);

            Storyboard.SetTarget(borderDeactivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderDeactivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderDeactivateStoryboard.Children.Add(borderDeactivateAnimation);

            heightUpAnimation = new DoubleAnimation(160, 370, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            heightUpAnimation.Completed += heightUpAnimation_Completed;
        }

        private void heightUpAnimation_Completed(object sender, EventArgs e)
        {
            SpecialThanksGrid.Visibility = Visibility.Visible;
            SpecialThanksGrid.BeginAnimation(OpacityProperty, fadeInAnimation);

            ShowSpecialThanksButton.BeginAnimation(OpacityProperty, fadeOutAnimation);
            ShowSpecialThanksButton.Visibility = Visibility.Collapsed;

            MinHeight = 370;
        }

        private void AboutWindow_OnActivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

            borderActivateStoryboard.Begin();
        }

        private void AboutWindow_OnDeactivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

            borderDeactivateStoryboard.Begin();
        }

        private void ShowSpecialThanksButton_OnClick(object sender, RoutedEventArgs e)
        {
            MaxHeight = 370;

            BeginAnimation(HeightProperty, heightUpAnimation);
        }
    }
}
