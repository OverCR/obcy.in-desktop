﻿using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ObcyInDesktop.Archive;
using ObcyInDesktop.Archive.Items;
using ObcyInDesktop.Filesystem;
using ObcyInDesktop.Localization;
using ObcyInDesktop.UI;
using ObcyProtoRev.Protocol.Client;
using Message = ObcyInDesktop.Archive.Items.Message;

namespace ObcyInDesktop.Windows
{
    public partial class ArchiveWindow
    {
        private readonly ColorAnimation glowFadeInAnimation;
        private readonly ColorAnimation glowFadeOutAnimation;

        private readonly Storyboard borderActivateStoryboard;
        private readonly Storyboard borderDeactivateStoryboard;

        private Thread fillThread;

        public ArchiveWindow()
        {
            InitializeComponent();

            glowFadeOutAnimation = new ColorAnimation(Colors.WindowActiveGlow, Colors.WindowInactiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            glowFadeInAnimation = new ColorAnimation(Colors.WindowInactiveGlow, Colors.WindowActiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            var borderActivateAnimation = new ColorAnimation(Colors.WindowInactiveBorder, Colors.WindowActiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            var borderDeactivateAnimation = new ColorAnimation(Colors.WindowActiveBorder, Colors.WindowInactiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            borderActivateStoryboard = new Storyboard();
            borderDeactivateStoryboard = new Storyboard();

            Storyboard.SetTarget(borderActivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderActivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderActivateStoryboard.Children.Add(borderActivateAnimation);

            Storyboard.SetTarget(borderDeactivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderDeactivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderDeactivateStoryboard.Children.Add(borderDeactivateAnimation);

            Title = LocaleSelector.GetLocaleString("ArchiveWindow_Title");

            LoadAllArchives();
        }

        private void LoadAllArchives()
        {
            var di = new DirectoryInfo(DirectoryGuard.ArchiveDirectory);
            var dirinfos = di.GetDirectories();

            foreach (var info in dirinfos)
            {
                ConversationFolders.Items.Add(
                    new ListBoxItem
                    {
                        Content = info.Name
                    }
               );
            }
        }

        private void ArchiveWindow_OnActivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

            borderActivateStoryboard.Begin();
        }

        private void ArchiveWindow_OnDeactivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

            borderDeactivateStoryboard.Begin();
        }

        private void ConversationFolders_OnSelected(object sender, SelectionChangedEventArgs e)
        {
            ConversationFiles.Items.Clear();

            var di = Directory.GetFiles(
                Path.Combine(
                    DirectoryGuard.ArchiveDirectory,
                    ((ListBoxItem)ConversationFolders.SelectedItem).Content.ToString()
                )
            );

            foreach (var path in di)
            {
                var name = Path.GetFileName(path);

                ConversationFiles.Items.Add(
                    new ListBoxItem
                    {
                        Content = name.Substring(0, name.Length - 4).Replace('.', ':')
                    }
                );
            }
        }

        private void ConversationFiles_OnSelected(object sender, SelectionChangedEventArgs e)
        {
            MessageLog.MessagePanel.Children.Clear();

            if (ConversationFiles.SelectedItem == null)
            {
                return;
            }

            var path = Path.Combine(
                DirectoryGuard.ArchiveDirectory,
                ((ListBoxItem)ConversationFolders.SelectedItem).Content.ToString(),
                ((ListBoxItem)ConversationFiles.SelectedItem).Content.ToString().Replace(':', '.') + ".log"
            );

            fillThread = new Thread(() => FillMessageLog(path));
            fillThread.SetApartmentState(ApartmentState.STA);
            fillThread.Start();
        }

        private void FillMessageLog(string path)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                using (var sr = new StreamReader(path))
                {
                    var array = sr.ReadToEnd().Split('\n');

                    foreach (var s in array)
                    {
                        object item;
                        try
                        {
                            item = Presence.Parse(s);
                        }
                        catch
                        {
                            try
                            {
                                item = Message.Parse(s);
                            }
                            catch
                            {
                                try
                                {
                                    item = Topic.Parse(s);
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }

                        if (item is Presence)
                        {
                            var p = item as Presence;
                            MessageLog.AddPresence(p.Connect);
                        }

                        if (item is Message)
                        {
                            var m = item as Message;
                            MessageLog.AddMessage(
                                new ObcyProtoRev.Protocol.Client.Message(
                                    MessageType.Chat,
                                    m.Body,
                                    null,
                                    null
                                    ),
                                m.Incoming,
                                m.Timestamp.FromUnixTimestamp()
                                );
                        }

                        if (item is Topic)
                        {
                            var t = item as Topic;
                            MessageLog.AddTopic(
                                new ObcyProtoRev.Protocol.Client.Message(
                                    MessageType.Topic,
                                    t.Body,
                                    null,
                                    null
                                    )
                                );
                        }
                    }
                }
            });
        }
    }
}
