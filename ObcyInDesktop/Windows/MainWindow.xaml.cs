﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ObcyInDesktop.Filesystem;
using ObcyInDesktop.Localization;
using ObcyInDesktop.Settings;
using ObcyInDesktop.UI;
using ObcyInDesktop.UI.Controls.ChatUI;
using ObcyProtoRev.Protocol.Client;
using Timer = System.Timers.Timer;

namespace ObcyInDesktop.Windows
{
    public partial class MainWindow
    {
        private bool firstEscPressed;
        private readonly Timer escTimer;

        private ChatView chatView;

        private DebugWindow debuggingWindow;
        private SettingsWindow settingsWindow;
        private AboutWindow aboutWindow;
        private ArchiveWindow archiveWindow;

        private readonly ColorAnimation glowFadeOutAnimation;
        private readonly ColorAnimation glowFadeInAnimation;

        private readonly Storyboard borderActivateStoryboard;
        private readonly Storyboard borderDeactivateStoryboard;

        private Thread blinkThread;

        public MainWindow()
        {
            InitializeComponent();

            CreateConnection();

            DirectoryGuard.DirectoriesSanityCheck();

            InitializeChatView();
            InitializeStatistics();

            glowFadeOutAnimation = new ColorAnimation(Colors.WindowActiveGlow, Colors.WindowInactiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            glowFadeInAnimation = new ColorAnimation(Colors.WindowInactiveGlow, Colors.WindowActiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            var borderActivateAnimation = new ColorAnimation(Colors.WindowInactiveBorder, Colors.WindowActiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            var borderDeactivateAnimation = new ColorAnimation(Colors.WindowActiveBorder, Colors.WindowInactiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            borderActivateStoryboard = new Storyboard();
            borderDeactivateStoryboard = new Storyboard();

            Storyboard.SetTarget(borderActivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderActivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderActivateStoryboard.Children.Add(borderActivateAnimation);

            Storyboard.SetTarget(borderDeactivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderDeactivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderDeactivateStoryboard.Children.Add(borderDeactivateAnimation);

            SettingsSelector.SettingsChanged += SettingsSelector_SettingsChanged;

            debuggingWindow = new DebugWindow(App.Connection);
            settingsWindow = new SettingsWindow();
            aboutWindow = new AboutWindow();
            archiveWindow = new ArchiveWindow();

            escTimer = new Timer(800);
            escTimer.Elapsed += escTimer_Elapsed;

            SettingsSelector.SetConfigurationValue("Voivodeship", SettingsSelector.GetConfigurationValue<string>("Voivodeship"));
        }

        private void escTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            firstEscPressed = false;
            escTimer.Stop();

            Application.Current.Dispatcher.Invoke(() =>
            {
                StartConversationButton.Content = LocaleSelector.GetLocaleString("LeftPanel_EndConversationButton");
            });
        }

        private void SettingsSelector_SettingsChanged(object sender, EventArgs e)
        {
            if (SettingsSelector.GetConfigurationValue<bool>("Appearance_UppercaseMenuHeaders"))
            {
                MainMenuButton.Label = LocaleSelector.GetLocaleString("Menu_File").ToUpper();
                OptionsMenuButton.Label = LocaleSelector.GetLocaleString("Menu_Options").ToUpper();
                ViewMenuButton.Label = LocaleSelector.GetLocaleString("Menu_View").ToUpper();
            }
            else
            {
                MainMenuButton.Label = LocaleSelector.GetLocaleString("Menu_File");
                OptionsMenuButton.Label = LocaleSelector.GetLocaleString("Menu_Options");
                ViewMenuButton.Label = LocaleSelector.GetLocaleString("Menu_View");
            }
        }

        private void CreateConnection()
        {
            App.Connection.OnlinePeopleCountChanged += connection_OnlinePeopleCountChanged;
            App.Connection.StrangerFound += connection_StrangerFound;
            App.Connection.ConversationEnded += connection_ConversationEnded;
            App.Connection.MessageReceived += connection_MessageReceived;
            App.Connection.SocketClosed += connection_SocketClosed;
            App.Connection.ServerClosedConnection += connection_ServerClosedConnection;
        }

        private void BlinkWindowGlow()
        {
            var isActive = false;
            var glow = Application.Current.Dispatcher.Invoke(() => (DropShadowEffect)FindName("BorderGlow"));

            for (var i = 0; i <= 3; i++)
            {
                if (isActive)
                    break;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (IsActive)
                        isActive = true;

                    if (glow != null)
                        glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

                    borderActivateStoryboard.Begin();
                });

                Thread.Sleep(1000);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (glow != null)
                        glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

                    borderDeactivateStoryboard.Begin();
                });
            }
        }

        private void InitializeChatView()
        {
            chatView = new ChatView(App.Connection)
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Height = Double.NaN,
                Width = Double.NaN
            };
            chatView.MessageSent += chatView_MessageSent;

            RightPanelArea.Child = chatView;
        }

        private void InitializeStatistics()
        {
            App.StatsManager.BestConversationTimeChanged += statsManager_BestConversationTimeChanged;
            App.StatsManager.ConversationCountChanged += statsManager_ConversationCountChanged;
            App.StatsManager.CurrentConversationTimeChanged += statsManager_CurrentConversationTimeChanged;
            App.StatsManager.KilometersCountChanged += statsManager_KilometersCountChanged;
            App.StatsManager.ReceivedMessagesCountChanged += statsManager_ReceivedMessagesCountChanged;
            App.StatsManager.SentMessagesCountChanged += statsManager_SentMessagesCountChanged;

            if (File.Exists(Path.Combine(DirectoryGuard.DataDirectory, DirectoryGuard.StatisticsFileName)))
            {
                App.StatsManager.LoadStats(
                    Path.Combine(DirectoryGuard.DataDirectory, DirectoryGuard.StatisticsFileName)
                );
            }
        }

        private void chatView_MessageSent(object sender, EventArgs e)
        {
            App.StatsManager.AddSentMessage();
        }

        private void connection_MessageReceived(object sender, Message message)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (!IsActive)
                {
                    blinkThread = new Thread(BlinkWindowGlow);
                    blinkThread.Start();

                    if(message.Type != MessageType.Service)
                        App.WindowFlashHelper.FlashApplicationWindow();
                }
            });
        }

        private void connection_ServerClosedConnection(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => StartConversationButton.IsEnabled = false);
        }

        private void connection_SocketClosed(object sender, string value)
        {
            Application.Current.Dispatcher.Invoke(() => StartConversationButton.IsEnabled = false);
        }

        private void statsManager_BestConversationTimeChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                BestConversationTimeTextBlock.Text =
                    string.Format("{0}:{1}:{2}",
                        App.StatsManager.Statistics.BestConversationTime.Hours.ToString("00"),
                        App.StatsManager.Statistics.BestConversationTime.Minutes.ToString("00"),
                        App.StatsManager.Statistics.BestConversationTime.Seconds.ToString("00")
                );
            });
        }

        private void statsManager_ConversationCountChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                TotalConversationCountTextBlock.Text = App.StatsManager.Statistics.ConversationCount.ToString("000000");
            });
        }

        private void statsManager_CurrentConversationTimeChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                CurrentConversationTimeTextBlock.Text =
                    string.Format("{0}:{1}:{2}",
                        App.StatsManager.CurrentConversationTime.Hours.ToString("00"),
                        App.StatsManager.CurrentConversationTime.Minutes.ToString("00"),
                        App.StatsManager.CurrentConversationTime.Seconds.ToString("00")
                    );
            });
        }

        private void statsManager_KilometersCountChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                TotalKilometersCountTextBlock.Text = App.StatsManager.Statistics.KilometersCount.ToString("000000");
            });
        }

        private void statsManager_ReceivedMessagesCountChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                TotalMessagesReceivedTextBlock.Text = App.StatsManager.Statistics.MessagesReceived.ToString("000000");
            });
        }

        private void statsManager_SentMessagesCountChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                TotalMessagesSentTextBlock.Text = App.StatsManager.Statistics.MessagesSent.ToString("000000");
            });
        }

        private void connection_ConversationEnded(object sender, DisconnectInfo disconnectInfo)
        {
            if (!disconnectInfo.IsReminder)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    StartConversationButton.Content = LocaleSelector.GetLocaleString("LeftPanel_StartConversationButton");
                });
            }
        }

        private void connection_StrangerFound(object sender, ContactInfo contactInfo)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                StartConversationButton.IsEnabled = true;
                StartConversationButton.Content = LocaleSelector.GetLocaleString("LeftPanel_EndConversationButton");

                if (SettingsSelector.GetConfigurationValue<bool>("Behavior_SendSexQueryOnStart"))
                {
                    chatView.SendMessage("km, wiek?");
                }
            });
        }

        private void connection_OnlinePeopleCountChanged(object sender, int count)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                PeopleCountTextBlock.Text = string.Format(LocaleSelector.GetLocaleString("LeftPanel_PeopleCountIndicator"), count);
            });
        }

        private void MainWindow_OnActivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (blinkThread != null)
                blinkThread.Abort();

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

            borderActivateStoryboard.Begin();
        }

        private void MainWindow_OnDeactivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

            borderDeactivateStoryboard.Begin();
        }

        private void StartConversationButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!App.Connection.IsStrangerConnected && !App.Connection.IsSearchingForStranger)
            {
                chatView.SearchForStranger();
                StartConversationButton.IsEnabled = false;
            }
            else if (App.Connection.IsStrangerConnected)
            {
                chatView.DisconnectFromStranger();
            }
        }

        private void ToggleCopyViewMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            chatView.ToggleCopyView();
            chatView.Toolbar.CopyViewToggleButton.IsChecked = !chatView.Toolbar.CopyViewToggleButton.IsChecked;
        }

        private void ToggleToolbarMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            chatView.ToggleToolbar();
        }

        private void ExitMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ArchiveManagerMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            // AlertWindow.Show(LocaleSelector.GetLocaleString("AlertWindow_IDontThinkSo"));
            if (archiveWindow.IsVisible)
            {
                archiveWindow.Activate();
            }
            else
            {
                archiveWindow = new ArchiveWindow();
                archiveWindow.Show();
            }
        }

        private void SettingsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (settingsWindow.IsVisible)
            {
                settingsWindow.Activate();
            }
            else
            {
                settingsWindow = new SettingsWindow();
                settingsWindow.ShowDialog();
            }
        }

        private void AboutApplicationMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (aboutWindow.IsVisible)
            {
                aboutWindow.Activate();
            }
            else
            {
                aboutWindow = new AboutWindow();
                aboutWindow.ShowDialog();
            }
        }

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) &&
                Keyboard.IsKeyDown(Key.LeftShift) &&
                Keyboard.IsKeyDown(Key.F12))
            {
                if (debuggingWindow.IsVisible)
                {
                    debuggingWindow.Activate();
                }
                else
                {
                    debuggingWindow = new DebugWindow(App.Connection);
                    debuggingWindow.Show();
                }
            }
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            debuggingWindow.Close();
            settingsWindow.Close();

            App.StatsManager.StopRecording();
            App.StatsManager.SaveStats(
                Path.Combine(DirectoryGuard.DataDirectory, DirectoryGuard.StatisticsFileName)
            );

            Environment.Exit(0);
        }

        private void ClearLogCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            chatView.ClearLog();
        }

        private void SwitchStrangerCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (App.Connection.IsStrangerConnected)
            {
                if (!firstEscPressed)
                {
                    escTimer.Start();

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        StartConversationButton.Content = LocaleSelector.GetLocaleString("LeftPanel_SureAboutDisconnect");
                    });
                    firstEscPressed = true;
                }
                else
                {
                    chatView.DisconnectFromStranger();
                    escTimer.Stop();
                    firstEscPressed = false;
                }
            }
            else
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    StartConversationButton.IsEnabled = false;
                });

                chatView.SearchForStranger();
            }
        }


        private void FlagStrangerCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            chatView.FlagStranger();
        }

        private void ToggleCopyViewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            chatView.ToggleCopyView();
            chatView.Toolbar.CopyViewToggleButton.IsChecked = !chatView.Toolbar.CopyViewToggleButton.IsChecked;
        }

        private void ToggleScrollingCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            chatView.ToggleLogScrolling();
            chatView.Toolbar.LogScrollingToggleButton.IsChecked = !chatView.Toolbar.LogScrollingToggleButton.IsChecked;
        }
    }
}
