﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ObcyInDesktop.Localization;
using ObcyInDesktop.UI;

namespace ObcyInDesktop.Windows
{
    public partial class AlertWindow
    {
        private readonly ColorAnimation glowFadeInAnimation;
        private readonly ColorAnimation glowFadeOutAnimation;

        private readonly Storyboard borderActivateStoryboard;
        private readonly Storyboard borderDeactivateStoryboard;

        public AlertWindow()
        {
            InitializeComponent();

            Title = LocaleSelector.GetLocaleString("AlertWindow_Title");


            glowFadeOutAnimation = new ColorAnimation(Colors.WindowActiveGlow, Colors.WindowInactiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            glowFadeInAnimation = new ColorAnimation(Colors.WindowInactiveGlow, Colors.WindowActiveGlow, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            var borderActivateAnimation = new ColorAnimation(Colors.WindowInactiveBorder, Colors.WindowActiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));
            var borderDeactivateAnimation = new ColorAnimation(Colors.WindowActiveBorder, Colors.WindowInactiveBorder, new Duration(new TimeSpan(0, 0, 0, 0, 150)));

            borderActivateStoryboard = new Storyboard();
            borderDeactivateStoryboard = new Storyboard();

            Storyboard.SetTarget(borderActivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderActivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderActivateStoryboard.Children.Add(borderActivateAnimation);

            Storyboard.SetTarget(borderDeactivateAnimation, WindowBorder);
            Storyboard.SetTargetProperty(borderDeactivateAnimation, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
            borderDeactivateStoryboard.Children.Add(borderDeactivateAnimation);
        }

        public static void Show(string message)
        {
            var wnd = new AlertWindow
            {
                MessageTextBlock = { Text = message }
            };
            wnd.ShowDialog();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AlertWindow_OnActivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeInAnimation);

            borderActivateStoryboard.Begin();
        }

        private void AlertWindow_OnDeactivated(object sender, EventArgs e)
        {
            var glow = (DropShadowEffect)FindName("BorderGlow");

            if (glow != null)
                glow.BeginAnimation(DropShadowEffect.ColorProperty, glowFadeOutAnimation);

            borderDeactivateStoryboard.Begin();
        }
    }
}
